import telebot
from telebot import types
import MySQLdb
import time

TOKEN = 'YOUR_TELEGRAM_BOT_TOKEN'
admin_username = "ADMIN_USERNAME"
db = MySQLdb.connect(host="localhost", user="username", passwd="password", db="database")
table = "TABLE" #table should have 4 rows: username (string), group_title (string) ,chat_id (int) ,user_id (int)
botname = "YOUR_BOT"

tb = telebot.TeleBot(TOKEN)
cursor = db.cursor()

@tb.message_handler(commands=['start'])
def command_start(message):
    username = message.from_user.username
    chat_id = message.chat.id
    from_user = message.from_user.id
    search = cursor.execute('SELECT * FROM '+str(TABLE)+' WHERE chat_id = "'+str(chat_id)+'"')
    if search == 0:      #not yet in table 
        try:  #is it a group?
            group_title = message.chat.title #only groups have message.chat.title
            #ONLY WORKS WITH ASCII CHARS: print('New subscriber! Group: '+str(group_title)+' has been added to the database.')
            tb.send_message(chat_id, "Congrats "+group_title+"! You are now subscribed.")
        except: #if no message.chat.title: single user chat
            group_title = "NOT A GROUP"
            tb.send_message(chat_id, "Congrats "+str(message.chat.first_name)+"! You are now subscribed.")
            print('New subscriber! User: '+str(username)+' has been added to the database.')
        cursor.execute('INSERT into '+str(TABLE)+' (username, grouptitle, chat_id, user_id) values (%s,%s,%s,%s)', (username, group_title, chat_id, from_user))
        db.commit()
        
    else:
        print(str(chat_id)+" already in database.")
        tb.send_message(chat_id, "You are already subscribed. Thank you :)")

@tb.message_handler(commands=['stop','unsubscribe'])
def command_stop(message):
    chat_id = message.chat.id
    cursor.execute('DELETE FROM '+str(TABLE)+' WHERE chat_id = "'+str(chat_id)+'"')
    db.commit()
    tb.send_message(chat_id,"You have been unsubscribed. Sad :(")
    print('chat_id: '+str(chat_id)+' has unsubscribed.')


@tb.message_handler(commands=['send'])
def command_send(message):
    if message.from_user.username == admin_username: #if username is admin allow sending message to all users
        cursor.execute('SELECT * from '+str(TABLE)+' WHERE 1')
        data = cursor.fetchall()
        text = message.text.replace('/send ','').replace('/send@'+str(botname)+' ', '')
        for row in data:
            chat_id = row[2] 
            tb.send_message(chat_id,text)
    else: #not an admin
        tb.send_message(message.chat.id,"ah ah ah you didn't say the magic word!")
        img = open("ahah.jpg", "rb")
        tb.send_photo(message.chat.id,img)

@tb.message_handler(commands=['subscribers'])
def command_subscribers(message):
    if message.from_user.username == admin_username: #if username is admin allow sending message to all users
        cursor.execute('SELECT COUNT(*) FROM '+str(TABLE)+' WHERE 1')
        data = cursor.fetchone() #data[0] contains count
        countall = data[0] #zero index contains count
        cursor.execute('SELECT COUNT(*) FROM '+str(TABLE)+' WHERE grouptitle = "NOT A GROUP"')
        data = cursor.fetchone() 
        countsingle = data[0]
        countgroup = countall - countsingle
        tb.send_message(message.chat.id, "This Bot has "+str(countall)+" subscribers ("+str(countsingle)+" single, "+str(countgroup)+" groups)", reply_to_message_id= message.message_id)
        #todo: add group and single user info
    else: #not an admin
        tb.send_message(message.chat.id,"ah ah ah you didn't say the magic word!")
        img = open("ahah.jpg", "rb")
        tb.send_photo(message.chat.id,img)

tb.polling(none_stop=True)

while True: # Don't let the main Thread end.
    time.sleep(0.0001) #reduce high cpu load of pass